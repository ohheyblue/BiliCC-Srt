# BiliCC-Srt

#### 介绍
用于下载BiliBili CC字幕的工具。
用QT5制作，编译发布有Windows、MacOS、Android，另有Python版。
（MacOS是虚拟机10.12做的，Android也只在Android6.0上测试过，二者大约会有兼容性问题。）

#### 小说明
-  **字幕路径** ：Win和Python版字幕文件位于软件根目录（软件所在的位置），MacOS在下载目录，Android在/sdcard/BILICC-SRT。
-  **查看BV号** ：URL https://www.bilibili.com/video/BV13t411U7Vm 中，BV号为1w4411P7xn或者BV1w4411P7xn（都可）。其他是EP号的，网页端播放器下面的简介中有BV号。
 
-  **Python版使用** ：一、安装Python3，二、安装Requests，三、执行BILIC.py。
 
-  **没有文件** ：应该是所在目录不够权限读写，请尝试以管理员身份运行或换目录。
-  **乱码问题** ：似乎只是暴风影音对UTF-8支持不太好，若有需要，可以尝试使用GB2312编码。
-  **无法启动** ：Win请安装"Microsoft Visual C++ 2015-2019 Redistributable"(百度“微软运行库合集”下载安装亦可)。
 
#### 蓝奏云
https://kgd.lanzoux.com/b00naiahe 密码:ahxs